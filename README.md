# TextureUnpacker

#### 介绍
解压 TexturePacker 打包后的素材，还原成独立资源。仅支持 python3 的运行环境

#### 使用说明

##### 初始化
安装依赖包

    pip install -r requirements.txt
    
##### 使用

    python TextureUnPacker.py sample.plist
    